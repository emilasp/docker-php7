#!/bin/bash

BUILD_BASE=$1

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`


restartSsh () {
    echo -n "$(tput setaf 1)$1:$(tput setaf 2) restart ssh..$(tput sgr0) "
    docker exec -it $(docker-compose ps | awk '{print $1}' | grep _$1_) service ssh restart
}

inspectIp () {
    echo -n "$(tput setaf 1) IP: $(tput setaf 2)$1$(tput sgr0) "
    docker inspect $(docker-compose ps | awk '{print $1}'  | grep _$1_) | grep '\"IPAddress\": \"1' | grep IP
}

echo -e "\n$(tput setaf 1)----STOP containers----$(tput sgr0)"
docker-compose -f docker-compose.dev.yml stop

echo -e "\n$(tput setaf 1)----RESTART containers----$(tput sgr0)"
docker-compose -f docker-compose.dev.yml up -d
echo -e "\n$(tput setaf 1)----LIST containers----$(tput sgr0)"
docker-compose ps

echo -e "\n$(tput setaf 1)----Restart SSH----$(tput sgr0)"

restartSsh 'php7'
restartSsh 'php'
restartSsh 'mysql'
restartSsh 'postgres'

echo -e "\n$(tput setaf 1)----Get IP----$(tput sgr0)"

inspectIp 'php7'
inspectIp 'php'
inspectIp 'mysql'
inspectIp 'postgres'
