# Please, see https://www.nginx.com/resources/wiki/start/topics/recipes/symfony/

server
{
    server_name dev.monetnik.ru www.dev.monetnik.ru;
    root /var/www/monetnik/web;
    error_log /var/log/nginx/dev.monetnik.ru.error.log notice;
    access_log /var/log/nginx/dev.monetnik.ru.access.log combined;

    location /
    {
        # try to serve file directly, fallback to app_dev.php
        try_files $uri /app_dev.php$is_args$args;

        # For PROD
        #try_files $uri /app.php$is_args$args;
    }

    location ~ ^/(app_dev|config)\.php(/|$)
    {
        # Для загрузки больших аттачей в форме оценки монет
        client_body_timeout 110s;
        client_max_body_size 512M;
        send_timeout 110s;

        include fastcgi_params;
        fastcgi_param fastcgi_read_timeout 600s;
        fastcgi_pass php-fpm:9000;

        fastcgi_split_path_info ^(.+\.php)(/.*)$;

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;

        fastcgi_read_timeout 3600s;
    }

    location ~ ^/app\.php(/|$)
    {
        fastcgi_pass php-fpm:9000;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path instead of the path to the symlink to PHP
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/app.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    location ~ ^/(css|js|img|bundles|storage|uploads|vendor)/
    {
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
   location ~ \.php$
   {
     return 404;
   }
}
