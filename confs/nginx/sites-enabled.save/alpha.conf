upstream php_alpha {
    server php7:9000;
}

server {

    proxy_read_timeout  900;
    client_header_timeout  10m;
    client_body_timeout    10m;
    send_timeout           10m;

    listen   80;
    server_name alpha.local, www.alpha.local;

    root /var/www/sites/alpha/frontend/web;

    access_log  /var/www/logs/alpha.local.nginx.access.log;
    error_log  /var/www/logs/alpha.local.nginx.errors.log;

    #expires     1m;
    add_header  Cache-Control public;

    client_max_body_size 5m;

    set $yii_bootstrap "index.php";
    charset utf-8;

    location ~ \.(js|css|png|jpg|gif|swf|ico|svg|ttf|eot|woff|pdf|mov|fla|zip|rar)$ {
        gzip_static on;

        gzip    on;
        gzip_min_length 1100;
        gzip_buffers 64 8k;
        gzip_comp_level 5;
        gzip_http_version 1.1;
        gzip_proxied any;
        gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

        try_files $uri =404;
        root /var/www/sites/alpha/frontend/web/; # Путь к корню вашего сайта
        access_log off; # не пишем логи
        expires 1w; # кешируем у клиента на 3 дня
    }

    location / {
        index  index.html $yii_bootstrap;
        try_files $uri $uri/ /$yii_bootstrap?$args;
    }

    location /ws {
        limit_conn perip 5; #делаем ограничение 5 вебсокетов на 1 ip

        proxy_pass http://workers:9001/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto https;
        proxy_read_timeout 86400s;
        proxy_send_timeout 86400s;
        proxy_redirect off;

    }

    location ~ \.php {
        fastcgi_split_path_info  ^(.+\.php)(.*)$;
        set $fsn /$yii_bootstrap;
        if (-f $document_root$fastcgi_script_name){
            set $fsn $fastcgi_script_name;
        }

	    fastcgi_pass php_alpha;

	    include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;

	    fastcgi_buffers 8 32k;
	    fastcgi_buffer_size 32k;

        fastcgi_param  PATH_INFO        $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED  $document_root$fsn;

        proxy_send_timeout 1200; # время ожидания при передаче запроса
        proxy_read_timeout 1200; # время ожидания при чтении ответа
        fastcgi_read_timeout 3600;
    }

    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }
}
