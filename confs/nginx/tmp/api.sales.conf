upstream php_sales_api {
    server php:9000;
}

server {
    client_max_body_size 100M;
    fastcgi_read_timeout 1800;

    proxy_read_timeout  900;
    client_header_timeout  10m;
    client_body_timeout    10m;
    send_timeout           10m;

    listen   80;
    server_name api.sales.local, www.api.sales.local;

    root /var/www/sites/tmp/api/web/;

    access_log  /var/www/logs/sales.api.local.nginx.access.log; 
    error_log  /var/www/logs/sales.api.local.nginx.errors.log; 

    #expires     1m;
    add_header  Cache-Control public;

    set $yii_bootstrap "index.php";
    charset utf-8;

    location ~ \.(js|css|png|jpg|gif|swf|ico|svg|ttf|eot|woff|pdf|mov|fla|zip|rar)$ {
        gzip_static on;
        try_files $uri =404;
        root /var/www/sites/tmp/api/web/; # Путь к корню вашего сайта
        access_log off; # не пишем логи
        #expires 8d; # кешируем у клиента на 3 дня
    }

    location / {
        index  index.html $yii_bootstrap;
        try_files $uri $uri/ /$yii_bootstrap?$args;
    }

    location ~ ^/(protected|framework|themes/\w+/views) {
        deny  all;
    }

    location ~ \.php {
        fastcgi_split_path_info  ^(.+\.php)(.*)$;
        set $fsn /$yii_bootstrap;
        if (-f $document_root$fastcgi_script_name){
            set $fsn $fastcgi_script_name;
        }

	    fastcgi_pass php_sales_api;

	    include fastcgi_params;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fsn;
       
	    fastcgi_buffers 8 32k;
	    fastcgi_buffer_size 32k;

        fastcgi_param  PATH_INFO        $fastcgi_path_info;
        fastcgi_param  PATH_TRANSLATED  $document_root$fsn;

        proxy_send_timeout 1200; # время ожидания при передаче запроса
        proxy_read_timeout 1200; # время ожидания при чтении ответа
        fastcgi_read_timeout 3600;
    }

    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }
}
 
