Образ сервера для DEV PHP 7
=============================



Installation
------------
Обновляем docker-compose
```bash
curl -L "https://github.com/docker/compose/releases/download/1.14.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

**Подготавливаем(из папки с докер пакетом):**
```bash
mkdir /var/www/logs/
mkdir /var/www/docker/
ln -s $(pwd)/confs /var/www/docker/
```

**Подготавливаем(из папки с докер пакетом):** MAC OS
```bash
mkdir /Users/emilfernando/www/logs/
mkdir /Users/emilfernando/www/docker/
ln -s $(pwd)/confs /Users/emilfernando/www/docker/
```

**Создаем образы dev:**
```bash
    docker-compose -f docker-compose.dev.yml  build
```

**Создаем образы prod(обновление базовых образов):**
```bash
docker-compose build
bash/docker-push
```

**PHPSTORM:**
```bash
php7.1 - /usr/local/bin/php

```




Usage
------------

Создаем контейнеры prod:
```bash
docker-compose up -d
```

Создаем контейнеры dev:
```bash
docker-compose -f docker-compose.dev.yml up -d
```

Останавливаем контейнеры:
```bash
docker-compose stop
```

Заходим в контейнер:
```bash
docker ps
docker exec -i -t fabd435b50b7 bash
```

Удаляем контейнеры:
```bash
docker rm $(docker ps -a -q)
```

Удаляем образы:
```bash
docker rmi $(docker images -q)
```

Смотрим информацию по запущенному контейнеру:
```bash
docker inspect php7dev_mysql_1
```


Смотрим логи по контейнеру(как вариант если он уходит в restarting):
```bash
docker logs php7dev_mysql_1
```


Добавляем внешний network:
```bash
docker network create --gateway 172.16.238.1 --subnet 172.16.238.0/24 static_ip_network
```

Удаляем network:
```bash
docker network rm static_ip_network
```

После создания и связывания контейнеров
========================
```bash
docker ps
```

PHP
-----
Общий баш скрипт для старта ssh:
```bash
ln -s /var/www/sites/php7dev/bash/docker_start /usr/bin/
docker_start
```

Запускаем ssh:
```bash
docker exec -i -t php7dev_php_1 bash /usr/sbin/sshd
```
Подключаемся по ssh:
```bash
docker inspect php7dev_php_1
```
Видим - "IPAddress": "172.17.0.8",
Подключаем XDEBUG, MD, CS
```bash
ssh root@172.17.0.8 -p 22
```
В настройках указываем путь до php - /usr/local/bin/php
Настраиваем XDEBUG
port 9001
KEY PHPSTORM

Настраиваем CS
1. Добавляем сервер PHP7
2. Прописываем путь до phpcs /usr/bin/phpcs

Настраиваем MD
1. Добавляем сервер PHP7
2. Прописываем путь до phpmd /usr/bin/phpmd

MySQL, MariaDB
-----

Запускаем ssh:
```bash
docker exec -i -t php7dev_mysql_1 bash /usr/sbin/sshd
```
Подключаемся по ssh:
```bash
docker inspect php7dev_mysql_1
```
Видим - "IPAddress": "172.17.0.8",
Подключаем BD UI
```bash
ssh root@172.17.0.8 -p 22
```

PostgreSQL
-----

После первого запуска создаём кластер с поддержкой русской кодировки:
####docker exec -i -t php7dev_postgres_1 pg_createcluster --locale ru_RU.UTF-8 --start 9.4 main

Запускаем ssh:
```bash
docker exec -i -t php7dev_postgres_1 bash /usr/sbin/sshd
```
Подключаемся по ssh:
```bash
docker inspect php7dev_postgres_1
```
Видим - "IPAddress": "172.17.0.8",
Подключаем BD UI
```bash
ssh root@172.17.0.8 -p 22
```
Добавляем базу
```bash
su postgres
psql
```
```sql
#CREATE USER alpha WITH password '';
CREATE DATABASE alpha WITH OWNER = postgres ENCODING = 'UTF8' TABLESPACE = pg_default LC_COLLATE = 'ru_RU.UTF-8' LC_CTYPE = 'ru_RU.UTF-8' CONNECTION LIMIT = -1;
#GRANT CONNECT, TEMPORARY ON DATABASE alpha TO public;
#GRANT ALL ON DATABASE alpha TO alpha;
\q
```

RabbitMQ
-----

Запускаем manager:
```bash
http://localhost:15672/
```
Подключаемся:
```bash
docker inspect php7dev_rebbitmq_1
```

Docker HUB
=======

push
------------
```bash
bash/docker-push
```

Info
=======

Volumes
------------
/var/www/docker/confs/nginx/sites-enabled

/var/www/docker/data/mariadb/database
/var/www/docker/data/mysql/database
/var/www/docker/data/pg/database

/var/www/docker/logs


Bugs and problems
=======
Device is Busy:
------------
```bash
service docker restart
docker-compose down
docker-compose down --remove-orphans
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker-compose -f docker-compose.dev.yml up -d
```

https://vincent.composieux.fr/article/run-a-symfony-application-using-docker-and-docker-compose