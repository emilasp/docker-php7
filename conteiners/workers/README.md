Контейнер с воркером
=============================



Installation
------------
**Создаём в папке проекта supervisor конфиг:**
```bash
touch /var/www/sites/abs-crm/console/workers/supervisor/queue.conf

[program:queueabs]
command= nohup bash -c "/var/www/sites/abs-crm/yii  queue/listen --verbose=1 --color=0"
process_name=%(program_name)s_%(process_num)02d
autostart=true
autorestart=true
user=www-data
numprocs=1
redirect_stderr=true
stdout_logfile=/var/www/sites/abs-crm/console/runtime/logs/yii-queue-worker.log
```

**Создаём ссылку на конфиг:**
```bash
ln -s /var/www/sites/abs-crm/console/workers/supervisor/workers.conf /var/www/docker/confs/workers/conf.d/
```

